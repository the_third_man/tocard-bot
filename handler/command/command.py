import re
from handler.irchandler import IrcHandler

class NotImplementedCmdHandler(Exception):
	pass

class CmdHandler(IrcHandler):
	def handle(self, cmd, arg):
		raise NotImplementedCmdHandler("command handle not implemented")

	def notice_registration(self, cmd_dispatcher):
		pass

class TestCmdHandler(CmdHandler):
	def __init__(self):
		super(TestCmdHandler, self).__init__()
		self.regexp = re.compile(r"[Tt]est\.?")

	def handle(self, cmd, arg):
		self.send("TEST WITH ARG:{0}".format(arg))


class QuitCmdHandler(CmdHandler):
	def __init__(self):
		super(EvalCmdHandler, self).__init__()
		self.regexp = re.compile(r"({[Q]uit|[dD]eco|[bB]ye)")

	def handle(self, cmd, arg):
		self.send("Bye !")
		exit()

		
