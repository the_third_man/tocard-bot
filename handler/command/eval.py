import re
from command import CmdHandler


class EvalCmdHandler(CmdHandler):
	allowed_func = ["dir",
		"dict", "type", "set", "list", "range", "pow", "ord", "hex",
		"oct", "bin", "int", "all", "any", "bool", "hash", "map", "reduce"
		]

	def __init__(self):
		super(EvalCmdHandler, self).__init__()
		self.regexp = re.compile(r"[eE]val")
		self.locals = {}
		for f in EvalCmdHandler.allowed_func:
			self.locals.update({f : eval(f)})

	def handle(self, cmd, arg):
		if (arg.count("import") or arg.count("__subclasses__") or
				arg.count("__bases__") or arg.count("__builtins__")):
			self.send("I can't sorry..")
			return
		try :
			res = eval(arg.strip() , {"__builtins__":None}, self.locals)
			self.send(res)
		except SyntaxError as e:
			self.send("Syntax Error !")

		except Exception as e:
			self.send(str(e))
		except SystemExit as e:
			self.send("I don't wanna die ='(")
