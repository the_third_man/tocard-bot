import re
from handler.message.msghandler import PrivMsgHandler

class CmdMsgHandler(PrivMsgHandler):
	def __init__(self):
		super(CmdMsgHandler, self).__init__()
		self.regexp = re.compile( ":(?P<nick>[^!]*)!\S* PRIVMSG (?P<dest>\S*) :(?P<msg>.*)")
		self.command_template = re.compile("!(?P<cmd>\S*) ?(?P<args>.*)")
		self.cmd_handlers = []

	def register(self, cmdhandler):
		self.cmd_handlers.append(cmdhandler)
		cmdhandler.notice_registration(cmdhandler)
	
	def notice_registration(self, bot):
		super(PrivMsgHandler, self).notice_registration(bot)
		self.bot = bot
	
	def handle(self, match):
		nick = match.group('nick')
		dest = match.group('dest')
		msg = match.group('msg')
		cmd_match = self.command_template.match(match.group('msg'))

		if cmd_match:
			cmd = cmd_match.group('cmd')
			args = cmd_match.group('args')
			print " RECV: cmd '{0}' with args '{1}'\n".format(cmd, args)

			if not bot.can_access(nick, cmd, arg):
				self.send("You don't have access to that command !")
				return
			
			for cmd_handler in self.cmd_handlers:
				if cmd_handler.match(cmd):
					def response_send(msg):
						self.send(msg, dest)
					cmd_handler.set_send(response_send)
					cmd_handler.handle(cmd, args)


