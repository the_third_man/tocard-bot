import re
from msghandler import PrivMsgHandler
from handler.command.command import CmdHandler


class CountMsgHandler(PrivMsgHandler): 
	def __init__(self):
		super(CountMsgHandler, self).__init__()
		self.regexp = re.compile( ":(?P<nick>[^!]*)!\S* PRIVMSG (?P<dest>\S*) :(?P<msg>.*)")
		self.words = ["mec", "bite", "roux", "tocard", "EPITA", "luc", "python"]
		self.score = {}
		for word in self.words:
			self.score.update({word : {}})


	def notice_registration(self, bot):
		super(CountMsgHandler, self).notice_registration(bot)
		bot.register_cmd(CountCmd(self))

	def handle(self, match):
		nick = match.group('nick')
		dest = match.group('dest')
		msg = match.group('msg').strip()
		for word in self.words: 
			count = msg.count(word)
			if count > 0:
				if nick not in self.score[word]:
					self.score[word][nick] = count
				else:
					self.score[word][nick] += count
				self.send("[{0}]: {1} point for word <{2}>".format(nick, self.score[word][nick], word), dest)


class CountCmd(CmdHandler):
	def __init__(self, counter):
		super(CountCmd, self).__init__()
		self.regexp = re.compile(r"[Cc]ount([sS]ore)?")
		self.counter = counter

	def handle(self, cmd, arg):
		if not arg:
			self.send("Need a word for score")
			return
		arg = arg.strip()
		if arg not in self.counter.score:
			self.send("Unknow word {0}".format(arg))
			return
	
		score = self.counter.score[arg]
		res = []
		podium = sorted(score, lambda x,y : cmp(score[x], score[y], reverse=True))
		
		for i in range(min(3, len(podium))):
			res.append("{0}) with {1} point => {2}".format(
				i + 1, score[podium[i]], podium[i]))
		self.send("|".join(res))
		
			
