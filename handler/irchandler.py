import re

class NotImplementedHandler(Exception):
	pass

class IrcHandler(object):
	def __init__(self):
		self.regexp = re.compile("")
		self.send = self.default_send 


	def match(self, msg):
		return self.regexp.match(msg)

	def handle(self, match):
		raise NotImplementedHandler("Handle method not implemented for {0}".format(type(self)))

	def notice_registration(self, bot):
		self.set_send(bot.send_msg)

	def default_send(self, *args):
		raise NotImplementedHandler("This handler {0} is not registered to a bot".format(self))

	def set_send(self, send_methode):
		self.send  = send_methode
