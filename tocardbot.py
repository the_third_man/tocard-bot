import sys
import socket
import string
import time

from handler.message import *
from handler.command import *

if "--test" in sys.argv:
  HOST="127.0.0.1"
else:
  HOST="irc.u-psud.rezosup.org"
PORT=6667
NICK="Tocard Leader"
IDENT="El.leader"
REALNAME="Tocard.Bot"

class IrcBot(object):
	def __init__(self, nick, ident, realn):
		self.nick = nick
		self.ident = ident
		self.realn= realn
		self.s = socket.socket()
		self.chans = []
		self.msg_handlers = []

		self.def_cmd_dispatch = CmdMsgHandler()
		self.register(self.def_cmd_dispatch)

	def connect(self, (HOST, PORT)):
		self.s.connect((HOST, PORT))
		self.s.send("NICK %s\r\n" % self.nick)
		self.s.send("USER %s %s bla :%s\r\n" % (self.ident, HOST, self.realn))


	def can_acces(self, nick, cmd, arg):
		return True

	def join(self, chan):
		self.chans.append(chan)
	
	def send_msg(self, msg):
		self.s.send(msg)

	def register(self, msghandler):
		self.msg_handlers.append(msghandler)
		msghandler.notice_registration(self)

	def register_cmd(self, cmdhandler):
		self.def_cmd_dispatch.register(cmdhandler)
		cmdhandler.notice_registration(self)

	def launch(self):
	 	self.s.recv(42000)
		for chan in self.chans:
			self.s.send("JOIN {0}\r\n".format(chan))
		data = "DATA"
		while data:
			data = self.s.recv(4096)
			if data:
				self.parse_msg(data)

	def parse_msg(self, msg):
		print "[RECV] msg :{0}".format(msg)
		for handler in self.msg_handlers:
			match = handler.match(msg)
			if match:
				handler.handle(match)





pingh = PingMsgHandler()
counth = CountMsgHandler()

testch = TestCmdHandler()
evalch = EvalCmdHandler()


tbot = IrcBot(NICK, IDENT, REALNAME)
tbot.connect((HOST,PORT))

tbot.register(pingh)
tbot.register(counth)

tbot.register_cmd(testch)
tbot.register_cmd(evalch)

tbot.join("#mytest")
#tbot.join("#tocard-stage")
tbot.launch()
